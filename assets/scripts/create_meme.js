const toptext = document.getElementById("toptext");
const bottomtext = document.getElementById("bottomtext");
const selectedImage = document.getElementById("selected_image");

const topTextDiv = document.getElementById("topTextDiv");
const bottomTextDiv = document.getElementById("bottomTextDiv");

const canvas = document.getElementById("canvas");

const btnSaveMeme = document.getElementById('btn-save-meme');
const hidUserId = document.getElementById('user_id');

const radIsPublic = document.getElementById('is_public');
const radIsNotPublic = document.getElementById('is_not_public');

const memeName = document.getElementById('name');

toptext.addEventListener("change", e => {
  topTextDiv.innerHTML = e.currentTarget.value;
});

bottomtext.addEventListener("change", e => {
  bottomTextDiv.innerHTML = e.currentTarget.value;
});

function selectImage(elem) {
  canvas.style.backgroundImage = "url('" + elem.src + "')";
  selectedImage.value = elem.getAttribute("data-id");
}

btnSaveMeme.onclick = (e) => {
  e.preventDefault();

  let data = new FormData();

  data.append("name", memeName.value);
  data.append("selected_image", selectedImage.value);
  data.append("toptext", toptext.value);
  data.append("bottomtext", bottomtext.value);
  data.append("user_id", hidUserId.value);
  
  let isPublic = '';

  if (radIsPublic.checked) {
    isPublic = radIsPublic.value;
  } else if (radIsNotPublic.checked) {
    isPublic = radIsNotPublic.value;
  }

  data.append('is_public', isPublic);

  fetch("../../controllers/process_create_meme.php", {
    method: "POST",
    body: data
  })
    .then(response => {
      return response.text();
    })
    .then(data_from_fetch => {
      if (data_from_fetch === "success") {
        window.alert('Successfully created meme');
        window.location.replace("../../views/home.php");
      } else if (data_from_fetch === 'fail') {
        window.alert('Failed to create meme');
        window.location.replace("");
      }
    });
};

const reset = document.getElementById("reset");

reset.addEventListener("click", e => {
  canvas.style.backgroundImage = "";
  topTextDiv.innerHTML = "";
  toptext.value = "";
  bottomtext.value = "";
  memeName.value = "";
  bottomTextDiv.innerHTML = "";
});
