CREATE DATABASE meme_maker;

CREATE TABLE users (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    username VARCHAR(160) NOT NULL,
    email VARCHAR(160) NOT NULL,
    password VARCHAR(160) NOT NULL,
    PRIMARY KEY(id) 
);

CREATE TABLE memes (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(160) NOT NULL,
    image VARCHAR(160) NOT NULL,
    top_text VARCHAR(255),
    bottom_text VARCHAR(255),
    is_public BIT NOT NULL,
    user_id INT UNSIGNED,
    PRIMARY KEY(id),
    FOREIGN KEY(user_id)
        REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

