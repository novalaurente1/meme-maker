<?php 

    require '../templates/template.php';

    function get_content() {
        $user_logged_in = $_SESSION["user"]["id"];

        if ($user_logged_in === '') {
            header("Location: ../views/login.php");
        }
        
        $images = file_get_contents("../assets/images.json");
        $images_array = json_decode($images, true);
    ?>
        <div class="container pt-5" style="display: flex; flex-direction: column; justify-content: center;">
        <div class="row">
            <div class="col">
                <div style="border: 3px solid #f2f2f2; display: flex; background-repeat: round; flex-direction: column; justify-content: space-between; width: 400px; height: 400px;" id="canvas">
                    <div id="topTextDiv" style="width: 100%; height: 100px;"></div>
                    <div id="bottomTextDiv" style="width: 100%; height: 100px;"></div>
                </div>
            </div>
            <div class="col">
                <div>
                    <form method="POST">
                    <p>Step 1: Select Image</p>

                    <?php foreach ($images_array as $image): ?>
                        <img src="<?php echo $image["image"]; ?>" style="width: 50px; height: 50px; margin: 0 5px 5px 5px" alt="random meme template" onclick="selectImage(this);" data-id="<?php echo $image['id']; ?>">
                    <?php endforeach; ?>
                    <p>Step 2: Add Text</p>
                        <input type="hidden" id="selected_image" name="selected_image" />
                    <div class="form-group">
                        <label for="toptext">Top Text</label>
                        <input type="text" name="toptext" class="form-control" id="toptext">
                    </div>
                    <div class="form-group">
                        <label for="bottomtext">Bottom Text</label>
                        <input type="text" name="bottomtext" class="form-control" id="bottomtext">
                    </div>

                    <p>Step 3: Name your Meme</p>
                    <div class="form-group">
                        <input type="text" name="name" id="name" class="form-control">
                        <input type="hidden" name="user_id" id="user_id" value="<?= $user_logged_in ?>">
                    </div>

                    <p>Step 4: Share to public?</p>
                    <div class="form-group">
                        <input type="radio" name="is_public" value="y" id="is_public" checked="checked" class="ml-5 form_control"> Yes, I'm proud of my meme! <br>
                        <input type="radio" name="is_public" value="n" id="is_not_public" class="ml-5 form_control"> No, I'll keep it to myself.
                    </div>
                    <button type="button" class="btn btn-secondary" id="reset">Reset</button>
                    <button type="submit" class="btn btn-success" id="btn-save-meme">Save Meme</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="../assets/scripts/create_meme.js"></script>
<?php
    };
 ?>