<?php require "../templates/template.php"; ?>

<?php function get_content() {
    require "../controllers/connection.php";
?>
    <div class="container">
        <h1 style="text-align:center !important;">My Memes</h1>
        <div class="row">         
                <?php 
                
                $user_logged_in = $_SESSION["user"]["id"];

                if ($user_logged_in === '') {
                    header("Location: ../views/login.php");
                }

                    $images = file_get_contents("../assets/images.json");

                    $images_array = json_decode($images, true);

                    $memes_query = "SELECT * FROM memes where is_public = 0 AND user_id = $user_logged_in";

                    $query = mysqli_query($conn, $memes_query);

                    foreach ($query as $key => $value) {

                        ?>
                            <div class="col-lg-4 py-2">
                                <div class="card">
                                    <div style="border: 3px solid #f2f2f2; display: flex; background-repeat: round; flex-direction: column; justify-content: space-between; width: 340px; height: 340px; background-image: url('../<?php echo $images_array[$value['image'] - 1]['image']  ?>')" id="canvas">
                                        <div id="topTextDiv" style="width: 100%; height: 100px;"><?php echo $value['top_text'] ?></div>
                                        <div id="bottomTextDiv" style="width: 100%; height: 100px;"><?php echo $value['bottom_text'] ?></div>
                                    </div>
                                    <div class="card-body">
                                        <h4 class="card-title"><?php echo $value['name'] ?></h4>
                                    </div>
                                </div>          
                            </div>

                        <?php
                    }
                ?>
            


        </div>
    </div>

<?php
}
?>


                                
                                
                       

            